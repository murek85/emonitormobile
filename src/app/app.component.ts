import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { PackageService } from './package/package.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  providers: [PackageService]
})
export class AppComponent implements OnInit {

  countPackages = 0;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private packageService: PackageService) {

    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {

    this.countPackages = this.packageService.countPackages();

    if (this.countPackages > 0) {
      this.router.navigate(['/', 'package', 'list']);
    }
  }
}
