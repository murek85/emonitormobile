import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

// angular material
import {
  MatAutocompleteModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatInputModule,
  MatRadioModule,
  MatSelectModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatMenuModule,
  MatSidenavModule,
  MatToolbarModule,
  MatListModule,
  MatGridListModule,
  MatCardModule,
  MatStepperModule,
  MatTabsModule,
  MatExpansionModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatChipsModule,
  MatIconModule,
  MatProgressSpinnerModule,
  MatProgressBarModule,
  MatDialogModule,
  MatTooltipModule,
  MatSnackBarModule,
  MatTableModule,
  MatSortModule,
  MatPaginatorModule
} from '@angular/material';

// covalent
import {
  CovalentCommonModule,
  CovalentMenuModule,
  CovalentLayoutModule,
  CovalentNotificationsModule,
  CovalentMediaModule,
  CovalentStepsModule,
  CovalentChipsModule,
  CovalentPagingModule,
  CovalentDataTableModule,
  CovalentJsonFormatterModule,
  CovalentSearchModule,
  CovalentDialogsModule,
  CovalentLoadingModule,
  CovalentExpansionPanelModule,
  CovalentMessageModule
} from '@covalent/core';
/* any other core modules */
// (optional) Additional Covalent Modules imports
import { CovalentHttpModule } from '@covalent/http';
import { CovalentHighlightModule } from '@covalent/highlight';
import { CovalentMarkdownModule } from '@covalent/markdown';
import { CovalentDynamicFormsModule } from '@covalent/dynamic-forms';

import { IonicModule } from '@ionic/angular';

import { HomePage } from './home.page';

@NgModule({
  imports: [
    // covalent
    CovalentCommonModule,
    CovalentMenuModule,
    CovalentLayoutModule,
    CovalentNotificationsModule,
    CovalentMediaModule,
    CovalentStepsModule,
    CovalentChipsModule,
    CovalentPagingModule,
    CovalentDataTableModule,
    CovalentJsonFormatterModule,
    CovalentSearchModule,
    CovalentDialogsModule,
    CovalentLoadingModule,
    CovalentExpansionPanelModule,
    CovalentMessageModule,
    // (optional) Additional Covalent Modules imports
    CovalentHttpModule.forRoot(),
    CovalentHighlightModule,
    CovalentMarkdownModule,
    CovalentDynamicFormsModule,

    // angular material
    MatAutocompleteModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatSelectModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatStepperModule,
    MatTabsModule,
    MatExpansionModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatChipsModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatDialogModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,

    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ])
  ],
  declarations: [HomePage]
})
export class HomePageModule {}
