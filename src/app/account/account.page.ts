import { Component, ModuleWithComponentFactories, OnInit } from '@angular/core';
import { PackageService } from '../package/package.service';

@Component({
  selector: 'app-account',
  templateUrl: 'account.page.html',
  styleUrls: ['account.page.scss'],
  providers: [PackageService]
})
export class AccountPage implements OnInit {

  customActionSheetOptions: any = {
    header: 'Czas odświeżania'
  };

  autorefresh = true;
  time = 1;
  times: any[] = [
    { name: '1 minuta', value: 1 },
    { name: '5 minut', value: 5 },
    { name: '10 minut', value: 10 },
    { name: '30 minut', value: 30 }
  ];
  onlywifi = false;

  couriers: any[];

  constructor(private packageService: PackageService) {

  }

  ngOnInit() {
    this.couriers = this.packageService.getCouriers();
    this.couriers.forEach((courier: any) => {
      this.packageService.checkConnection(courier).then((status: any) => {
        courier.status = status;
      });
    });
  }

  checkStatus(courier) {
    return courier.status ? courier.status : 'Błąd połączenia';
  }

  checkStatusIcon(courier) {
    return courier.status ? 'mdi-check-circle-outline tc-green-200' : 'mdi-close-circle-outline tc-red-200';
  }
}
