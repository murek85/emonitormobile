import { Component, OnInit, AfterContentInit, AfterViewInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { TdDialogService } from '@covalent/core';
import { AlertController } from '@ionic/angular';
import { PackageService } from '../package.service';

import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';

@Component({
  selector: 'app-list-package',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss'],
  providers: [PackageService]
})
export class ListPackagePage implements OnInit, AfterContentInit, AfterViewInit {

  packages: any[];
  countPackages = 0;

  private db: SQLiteObject;

  constructor(private packageService: PackageService,
    private alertController: AlertController,
    private sqlite: SQLite) {

    this.packages = [];
  }

  initDb() {
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
    .then((db: SQLiteObject) => {
      this.db = db;

      this.db.executeSql('create table danceMoves(name VARCHAR(32))')
        .then(() => console.log('Executed SQL'))
        .catch(e => console.log(e));
    })
    .catch(e => console.log(e));
  }

  ngOnInit(): void {
    console.log('ngOnInit');
    this.initDb();
  }

  ngAfterContentInit() {
    console.log('ngAfterContentInit');
  }

  ngAfterViewInit() {
    console.log('ngAfterViewInit');

    this.countPackages = this.packageService.countPackages();

    this.packages = this.packageService.getPackages();
    this.packages.forEach((item: any) => {
      
      this.getStatusPackage(item).then((data: any) => {
        item.data = data;
      });
    });
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Ostrzeżenie',
      message: `Funkcjonalność dodawania przesyłek kodem kreskowym jest niedostępna.`,
      buttons: ['OK']
    });

    await alert.present();
  }

  barcode(): void {
    // this.presentAlert();
  }

  async getStatusPackage(item: any) {
    return await this.packageService.checkStatusPackage(item);
  }

  checkStatusPackageIcon(item: any) {
    switch (item.status.type) {
      // inpost
      case 'delivered': {
        return 'mdi-package-variant-closed tc-green-400';
      }
      
      case 'unknown': {
        return 'mdi-package-variant-closed tc-red-400';
      }

      default: {
        return 'mdi-cube-send tc-orange-400';
      }
    }
  }
}
