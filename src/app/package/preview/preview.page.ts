import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { TdDialogService } from '@covalent/core';
import { PackageService } from '../package.service';

@Component({
  selector: 'app-preview-package',
  templateUrl: 'preview.page.html',
  styleUrls: ['preview.page.scss'],
  providers: [PackageService]
})
export class PreviewPackagePage implements OnInit {
  
  // numer przesyłki
  number;

  // paczka
  package: any;
  address: any;
  history: any[];

  constructor(private dialogService: TdDialogService,
    private router: Router,
    private route: ActivatedRoute,
    private packageService: PackageService) {

      this.history = [];
  }

  ngOnInit() {

    this.number = this.route.snapshot.params['id'];
    this.package = this.packageService.getPackage(this.number);
    this.packageService.getAddressPackage(this.package).then((address: any) => this.address = address);
    this.packageService.getHistoryPackage(this.package).then((history: any) => this.history = history);
  }

  checkStatusPackageIcon(status: any) {
    switch (status.type) {
      // inpost
      case 'delivered': {
        return 'mdi-package-variant-closed tc-green-400';
      }
      case 'confirmed': {
        return 'mdi-package-variant-closed tc-indigo-400';
      }
      
      case 'unknown': {
        return 'mdi-cube tc-red-400';
      }

      default: {
        return 'mdi-cube-send tc-orange-400';
      }
    }
  }
}
