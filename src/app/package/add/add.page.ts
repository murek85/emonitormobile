import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { TdDialogService } from '@covalent/core';
import { PackageService } from '../package.service';

@Component({
  selector: 'app-add-package',
  templateUrl: 'add.page.html',
  styleUrls: ['add.page.scss'],
  providers: [PackageService]
})
export class AddPackagePage implements OnInit {
  // formularz
  formNumber;

  // numer przesyłki
  number;
  // nazwa
  name;
  // kurier
  courier;

  couriers: any[];

  constructor(private dialogService: TdDialogService,
    private router: Router,
    private packageService: PackageService,
    private alertController: AlertController) {
  }

  ngOnInit() {
    this.couriers = this.packageService.getCouriers();

    this.formNumber = new FormGroup({
      courier: new FormControl(''),
      name: new FormControl('', [Validators.required]),
      number: new FormControl('', [Validators.required])
    });
  }

  getErrorMessage(name: string) {
    switch (name) {
      case 'name': {
        return this.formNumber.controls.name.hasError('required') ? 'Pole wymaga wprowadzenia nazwy przesyłki' : '';
      }
      case 'number': {
        return this.formNumber.controls.number.hasError('required') ? 'Pole wymaga wprowadzenia numeru przesyłki' : '';
      }
    }

    return '';
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Informacja',
      message: `Podany numer przesyłki ${this.number} został już wprowadzony.`,
      buttons: ['OK']
    });

    await alert.present();
  }

  onSubmit() {
    const item: any = this.packageService.checkPackage(this.number, this.courier);
    if (item) {
      this.presentAlert();
      return;
    }

    this.packageService.addPackage({ 
      name: this.name,
      number: this.number,
      courier: this.courier
    });
    this.router.navigate(['package', 'list']);
  }
}
