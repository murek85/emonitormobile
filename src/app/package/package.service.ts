import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class PackageService {

    private packages: any[];

    private couriersDict: any[] = [
        { name: 'DHL', value: 'dhl', icon: 'assets/icon/dhl.png', active: false },
        { name: 'DPD', value: 'dpd', icon: 'assets/icon/dpd.png', active: false },
        { name: 'FedEx', value: 'fedex', icon: 'assets/icon/fedex.png', active: false },
        { name: 'GLS', value: 'gls', icon: 'assets/icon/gls.png', active: false },
        { name: 'Inpost', value: 'inpost', icon: 'assets/icon/inpost.png', active: true },
        { name: 'K-EX', value: 'kex', icon: 'assets/icon/kex.png', active: false },
        { name: 'Paczkomaty Inpost', value: 'paczkomaty', icon: 'assets/icon/paczkomaty.png', active: true },
        { name: 'Poczta Polska', value: 'pocztapolska', icon: 'assets/icon/pocztapolska.png', active: true },
        { name: 'UPS', value: 'ups', icon: 'assets/icon/ups.png', active: false },
        { name: 'Paczka w Ruchu', value: 'paczkaruch', icon: 'assets/icon/paczkaruch.png', active: false }
    ];

    constructor(private httpClient: HttpClient) {
        
        this.packages = [];
    }

    public addPackage(data: any): void {
        const packages = JSON.parse(localStorage.getItem('packages'));
        if (packages) {
            this.packages = packages
        }

        this.packages.push({ 
            courier: data.courier,
            name: data.name,
            number: data.number,
            data: {
                status: {
                    type: 'unknown',
                    name: 'Nieznany status'
                }
            }
        });

        localStorage.setItem('packages', JSON.stringify(this.packages));
    }

    public removePackage(): void {

    }

    public countPackages(): number {
        const packages: any[] = JSON.parse(localStorage.getItem('packages'));
        if (packages) {
            return packages.length;
        }

        return 0;
    }

    public getPackages(): any[] {
        const packages = JSON.parse(localStorage.getItem('packages'));
        if (packages) {
            this.packages = packages
        }
        return this.packages;
    }

    public getPackage(number: any): any {
        const packages = JSON.parse(localStorage.getItem('packages'));
        if (packages) {
            this.packages = packages
            return this.packages.find((item: any) => item.number === number);
        }

        return null;
    }

    public checkPackage(number: any, courier: any): any {
        const packages = JSON.parse(localStorage.getItem('packages'));
        if (packages) {
            this.packages = packages
            return this.packages.find((item: any) => item.number === number && item.courier.value === courier.value);
        }

        return null;
    }

    public async checkStatusPackage(item: any) {
        let data: any = { status: { type: 'unknown', name: 'Nieznany status' } };
        switch (item.courier.value) {
            case 'inpost':
            case 'paczkomaty': {
                data = await this.getInpost(item.number);
                break;
            }
        }

        return data;
    }

    public async getHistoryPackage(item: any) {
        let data: any[] = [];
        switch (item.courier.value) {
            case 'inpost':
            case 'paczkomaty': {
                data = await this.getHistoryInpost(item.number);
                break;
            }
        }

        return data;
    }

    public async getAddressPackage(item: any) {
        let data: any;
        switch (item.courier.value) {
            case 'inpost':
            case 'paczkomaty': {
                data = await this.getAddressInpost(item.number);
                break;
            }
        }

        return data;
    }

    public getCouriers(): any[] {
        return this.couriersDict;
    }

    public async getStatuses(item: any) {
        let statuses: any[] = [];
        switch (item.courier.value) {
            case 'inpost': 
            case 'paczkomaty': {
                statuses = await this.getStatusesInpost();
                break;
            }
        }

        return statuses;
    }

    private async getInpost(number: string) {
        let inpost: any = {};
        let response: any = await this.httpClient.get('https://api-shipx-pl.easypack24.net/v1/tracking/' + number).toPromise();

        if (response) {
            let statuses: any[] = await this.getStatusesInpost();
            let stat: any = statuses.find((status: any) => status.type === response.status);

            inpost = { status: stat, date: response.updated_at };
        }

        return inpost;
    }

    private async getHistoryInpost(number: string) {
        let history: any[] = [];
        let response: any = await this.httpClient.get('https://api-shipx-pl.easypack24.net/v1/tracking/' + number).toPromise();

        if (response) {
            let statuses: any[] = await this.getStatusesInpost();
            response.tracking_details.forEach((item: any) => {
                let stat = statuses.find((status: any) => status.type === item.status);

                history.push({
                    datetime: item.datetime,
                    status: stat
                });
            });
        }

        return history;
    }

    private async getAddressInpost(number: string) {
        let address: any = {};
        let response: any = await this.httpClient.get('https://api-shipx-pl.easypack24.net/v1/tracking/' + number).toPromise();

        if (response) {
            address = {
                line1: `${response.custom_attributes.target_machine_detail.address.line1}`,
                line2: `${response.custom_attributes.target_machine_detail.address.line2}`,
                description: response.custom_attributes.target_machine_detail.location_description
            };
        }

        return address;
    }

    private async getStatusesInpost() {
        let statuses: any[] = [];
        let response: any = await this.httpClient.get('https://api-shipx-pl.easypack24.net/v1/statuses').toPromise();

        if (response) {
            response.items.forEach((status: any) => {
                statuses.push({
                    type: status.name,
                    name: status.title
                });
            });
        }

        return statuses;
    }

    public async checkConnection(courier: any) {
        switch (courier.value) {
            case 'inpost':
            case 'paczkomaty': {
                let response: any = await this.httpClient.get('https://api-shipx-pl.easypack24.net.').toPromise();

                if (response) {
                    return response.version;
                }
            }
            default:
                return null;
        }
    }
}