import { NgModule, ModuleWithProviders, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

// angular material
import {
  MatAutocompleteModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatInputModule,
  MatRadioModule,
  MatSelectModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatMenuModule,
  MatSidenavModule,
  MatToolbarModule,
  MatListModule,
  MatGridListModule,
  MatCardModule,
  MatStepperModule,
  MatTabsModule,
  MatExpansionModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatChipsModule,
  MatIconModule,
  MatProgressSpinnerModule,
  MatProgressBarModule,
  MatDialogModule,
  MatTooltipModule,
  MatSnackBarModule,
  MatTableModule,
  MatSortModule,
  MatPaginatorModule
} from '@angular/material';

// covalent
import {
  CovalentCommonModule,
  CovalentMenuModule,
  CovalentLayoutModule,
  CovalentNotificationsModule,
  CovalentMediaModule,
  CovalentStepsModule,
  CovalentChipsModule,
  CovalentPagingModule,
  CovalentDataTableModule,
  CovalentJsonFormatterModule,
  CovalentSearchModule,
  CovalentDialogsModule,
  CovalentLoadingModule,
  CovalentExpansionPanelModule,
  CovalentMessageModule
} from '@covalent/core';
/* any other core modules */
// (optional) Additional Covalent Modules imports
import { CovalentHttpModule } from '@covalent/http';
import { CovalentHighlightModule } from '@covalent/highlight';
import { CovalentMarkdownModule } from '@covalent/markdown';
import { CovalentDynamicFormsModule } from '@covalent/dynamic-forms';

import { IonicModule } from '@ionic/angular';

import { PackagePage } from './package.page';
import { AddPackagePage } from './add/add.page';
import { ListPackagePage } from './list/list.page';
import { PreviewPackagePage } from './preview/preview.page';

import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';

@NgModule({
  imports: [
    // covalent
    CovalentCommonModule,
    CovalentMenuModule,
    CovalentLayoutModule,
    CovalentNotificationsModule,
    CovalentMediaModule,
    CovalentStepsModule,
    CovalentChipsModule,
    CovalentPagingModule,
    CovalentDataTableModule,
    CovalentJsonFormatterModule,
    CovalentSearchModule,
    CovalentDialogsModule,
    CovalentLoadingModule,
    CovalentExpansionPanelModule,
    CovalentMessageModule,
    // (optional) Additional Covalent Modules imports
    CovalentHttpModule.forRoot(),
    CovalentHighlightModule,
    CovalentMarkdownModule,
    CovalentDynamicFormsModule,

    // angular material
    MatAutocompleteModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatSelectModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatStepperModule,
    MatTabsModule,
    MatExpansionModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatChipsModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatDialogModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,

    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: PackagePage
      },
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
      },
      {
        path: 'list',
        component: ListPackagePage
      },
      {
        path: 'add',
        component: AddPackagePage
      },
      {
        path: ':id',
        component: PreviewPackagePage
      }
    ])
  ],
  declarations: [
    PackagePage,
    ListPackagePage,
    PreviewPackagePage,
    AddPackagePage
  ],
  providers: [
    SQLite
  ]
})
export class PackagePageModule {}
