# ** # Ionic4 / Angular6 / Angular CLI / Covalent UI / Angular Material2 / Typescript 2 / SASS ** #


### Screenshots
![Start](/images/2018-08-30%2017_14_00-Ionic%20Lab-Start.png)
![Add](/images/2018-08-30%2017_23_49-Ionic%20Lab-Add.png)
![List](/images/2018-08-30%2017_20_35-Ionic%20Lab-List.png)
![Preview](/images/2018-08-30%2017_22_07-Ionic%20Lab-Preview.png)
![Settings](/images/2018-08-30%2017_23_11-Ionic%20Lab-Settings.png)
